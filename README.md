# soal-shift-sisop-modul-2-F08-2022
**<br>1. Reza Maranelo Alifiansyah (5025201071)**
**<br>2. Natya Madya Marciola (5025201238)**
**<br>3. Khuria Khusna (5025201053)**

## Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.

a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `“/home/[user]/shift2/drakor”`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam `“/drakor/romance”`, jenis drama korea action akan disimpan dalam “/drakor/action” , dan seterusnya.

c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: `“/drakor/romance/start-up.png”`.

d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama `“start-up;2020;romance_the-k2;2016;action.png”` dipindah ke folder `“/drakor/romance/start-up.png”` dan `“/drakor/action/the-k2.png”`. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

e. Di setiap folder kategori drama korea buatlah sebuah file `"data.txt"` yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

### Penyelesaian Soal
> ***Note :*** Semua kode yang tertera merupakan potongan kode dan kemungkinan tidak berurutan

Sebelum membahas lebih lanjut, hal pertama yang bisa dilakukan adalah mendeklarasikan ataupun menginisialisasi variabel-variabel yang akan memudahkan dalam proses mengoding sebagai berikut.
```ruby
char path[50] = "/home/natya/Modul2/shift2/drakor";
char temp_path[100], old_file[100], new_file[100], tmp[100];
int count;
```
**Nomor 2a**
<br> Hal pertama yang diminta pada soal adalah mengextract zip ke dalam folder `“/home/[user]/shift2/drakor”` dan menghapus folder-folder yang tidak dibutuhkan. Pada soal ini diketahui bahwa file yang penting hanyalah file yang berbentuk .png. Pertama-tama, saya membuat sebuah fungsi `makedir` untuk membuat folder sebagai berikut.
```ruby
void makedir(char *path){
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/usr/bin/mkdir", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
Karena nantinya file yang diextract dimasukkan ke dalam folder `“/home/[user]/shift2/drakor”`, maka saya membuat sebuah folder sesuai yang diminta soal pada fungsi main dengan menjalankan fungsi `makedir` yang dibuat tadi.
```ruby
makedir(path);
```
> Dimana path yang dimaksud di sini adalah "/home/natya/Modul2/shift2/drakor"

Kemudian saya juga membuat sebuah fungsi bernama `unzip` untuk mengextract sebuah zip. Lalu mengeksekusinya di dalam fungsi main dengan `unzip();`. Pada fungsi ini yang diextract hanyalah file yang berbentuk `.png`.
```ruby
void unzip(){
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
        // this is child
        char *argv[] = {"unzip","-q" ,"/home/natya/Modul2/drakor.zip","*.png","-d","/home/natya/Modul2/shift2/drakor", NULL};
        execv("/bin/unzip", argv);	
    } else {
        // this is parent
        while ((wait(&status)) > 0);
    }
}
```
**Nomor 2b**
<br>*Beberapa fungsi yang saya buat dan digunakan pada sub soal ini*<br>
`Fungsi makedir`
```ruby
void makedir(char *path){
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", path, NULL};
        execv("/usr/bin/mkdir", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
`Fungsi make_file`
```ruby
void make_file(char *path, char *genre){
    FILE *fp;
    strcpy(tmp, path);
    strcat(tmp, "/data.txt");
    fp = fopen (tmp, "a+");
    fprintf(fp, "kategori : %s\n\n\n", genre);
    fclose(fp);
}
```
<br>Inti persoalan pada sub soal 2b adalah membuat folder untuk setiap jenis drama korea yang ada di dalam zip. Misalnya jenis drama korea romance akan disimpan dalam `"/drakor/romance` dan seterusnya. Untuk menyelesaikan soal ini, saya menggunakan directory listing untuk melihat secara rekursif seluruh file yang ada dalam suatu directory/folder. Pada soal ini file yang akan dilihat berada dalam folder `"/home/natya/Modul2/shift2/drakor"`. Berikut merupakan **potongan kode** yang saya buat untuk menjawab soal ini sekaligus menjawab sub soal selanjutnya.
```ruby
DIR *d,*exists;
struct dirent *dir;
char f_name[200], temp[200];
d = opendir(path);
if (d)
{
    while ((dir = readdir(d)) != NULL)
    {
        if(strstr(dir->d_name,"png")){
        strcpy(f_name, dir->d_name);
        strcpy(temp, dir->d_name);
        strcpy(temp_path, path);
                                
        //untuk hapus .pngnya
        int l = strlen(temp);
        for(int i = l, j = 4; j >= 0; j--,i--) temp[i] = '\0';
                                
        char * token = strtok(temp, ";_");
        //printf("%s\n", f_name);
                            
        if(!(strstr(dir->d_name,"_"))){
            count = 0;
            //string[0] nyimpen judul, string[1] nyimpen tahun, string[2] nyimpen genre
            char string[3][100];
            while(token != NULL){
                //printf ("%s ",token);
                strcpy(string[count],token);
                //biar ga looping
                token = strtok(NULL, ";_");
                count++;t
            }
            strcat(temp_path,"/");
            strcat(temp_path,string[2]);
                                
            exists = opendir(temp_path);
            if(!exists) {makedir(temp_path);make_file(temp_path, string[2]);}
            input_data(temp_path, string[0], string[1]);
						
            //old_file
            strcpy(old_file, path);
            strcat(old_file, "/");
            strcat(old_file, f_name);
							
            //new_file
            strcpy(new_file, temp_path);
            strcat(new_file, "/");
            strcat(new_file, string[0]);
            strcat(new_file, ".png");
										
            move_file(old_file, new_file);
							
        }
```
```ruby
else if(strstr(dir->d_name,"_")){
    count = 0;
    //string[0] nyimpen judul1, string[1] nyimpen tahun1, string[2] nyimpen genre1, string[3] nyimpen judul2, string[4] nyimpen tahun2, string[5] nyimpen genre1
    char string[6][100];
    while(token != NULL){
        //printf ("%s ",token);
							
        strcpy(string[count],token);
        //biar ga looping
        token = strtok(NULL, ";_");
        count++;
    }
												
    //old_file
	strcpy(old_file, path);
	strcat(old_file, "/");
	strcat(old_file, f_name);
							
	//new_file (yang awal)
	strcat(temp_path,"/");
	strcat(temp_path,string[2]);
							
	exists = opendir(temp_path);
	if(!exists) {makedir(temp_path);make_file(temp_path, string[2]);}
					
	input_data(temp_path, string[0], string[1]);
							
	strcpy(new_file, temp_path);
	strcat(new_file, "/");
	strcat(new_file, string[0]);
	strcat(new_file, ".png");
							
	copy_file(old_file, new_file);
							
	//new_file (yang akhir)
	strcpy(temp_path, path);
	strcat(temp_path,"/");
	strcat(temp_path,string[5]);
							
	exists = opendir(temp_path);
	if(!exists) {makedir(temp_path);make_file(temp_path, string[5]);}
	input_data(temp_path, string[3], string[4]);	
	strcpy(new_file, temp_path);
	strcat(new_file, "/");
	strcat(new_file, string[3]);
	strcat(new_file, ".png");

	move_file(old_file, new_file);						
}
```
**Algoritma kode**
- Pertama, program akan menelurusi file satu persatu. Kemudian file yang ditelurusi hanya file dengan tipe .png. 
- Kedua, nama dari file tersebut akan disimpan ke dalam beberapa variabel yang nantinya akan digunakan untuk memudahkan proses pengkodingan
- Ketiga, program akan menghapus `.png` dari nama file dengan cara menghapus 4 karakter terakhir dari array nama tersebut.
- Lalu akan digunakan manipulasi string menggunakan `strtok` untuk memisahkan sebuah string menjadi beberapa bagian sesuai dengan yang diinginkan. Dalam soal ini, terdapat dua tipe file dimana kedua tipe ini memiliki penamaan file yang berbeda pula. Tipe 1 adalah file yang berisi 1 poster dan Tipe 2 adalah file yang berisi 2 poster. Contoh penamaan tipe 1 : `start-up;2020.png`. Contoh penamaan tipe 2 : `start-up;2020;romance_the-k2;2016;action.png`.
- Maka perilaku untuk kedua file ini akan berbeda. Pada tipe 1 yang disimpan hanya 3 nilai, yaitu judul, tahun, dan genre. Sedangkan pada tipe 2 karena terdapat 2 poster, maka yang disimpan akan berjumlah 2x dari tipe 1, yaitu akan ada 6 nilai yang disimpan untuk masing-masing poster. 
- Untuk tipe 1, genre akan disimpan pada array indeks ke-2, sedangkan untuk tipe 2 akan disimpan pada array indeks ke-2 dan ke-5.
- Kemudian akan dicek, apakah folder dengan genre tersebut sudah ada atau tidak, jika tidak maka program akan membuat sebuah folder baru menggunakan fungsi `makedir` sekaligus membuat file 'data.txt' (yang akan digunakan pada sub soal 2E) dengan fungsi `make_file`.

Contoh folder yang sudah dikategorikan.
![error1](img/1.png)

**Nomor 2c**
<br>*Fungsi yang saya buat dan digunakan pada sub soal ini*<br>
`Fungsi move_file`
```ruby
void move_file(char *old_file, char *new_file) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mv", old_file, new_file, NULL};
        execv("/usr/bin/mv", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
Setelah folder selesai dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dengan genrenya dan akan direname dengan nama baru. Contohnya : `“/drakor/romance/start-up.png”`. Mengacu pada potongan kode yang ada pada **2B**, kode spesifik yang digunakan untuk menjawab sub soal ini adalah sebagai berikut.
```ruby
//old_file
strcpy(old_file, path);
strcat(old_file, "/");
strcat(old_file, f_name);
							
//new_file
strcpy(new_file, temp_path);
strcat(new_file, "/");
strcat(new_file, string[0]);
strcat(new_file, ".png");
										
move_file(old_file, new_file);
```
**Algoritma kode**
- Pertama, saya membuat dua path. Path pertama adalah path dimana file tersebut berada pada saat ini yang dinamai dengan old_file, sedangkan path kedua adalah path dimana folder tersebut akan dipindahkan sekaligus berisi nama baru dari file tersebut, path kedua saya namai dengan new_file. Saya membuat kedua path ini menggunakan manipulasi string. `strcpy` digunakan untuk mengcopy string dari satu variabel ke variabel lainnya. Sedangkan `strcat` digunakan untuk menggabungkan dua string menjadi satu.
- Kemudian, setelah kedua path sudah terbentuk, langkah selanjutnya hanya perlu menggunakan fungsi move_file yang sudah saya buat tadi untuk memindahkan file tersebut

Contoh isi dari folder yang berisi yang sudah dipindahkan.
![error1](img/2.png)

**Nomor 2d**
<br>*Beberapa fungsi yang saya buat dan digunakan pada sub soal ini*<br>
`Fungsi move_file`
```ruby
void move_file(char *old_file, char *new_file) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mv", old_file, new_file, NULL};
        execv("/usr/bin/mv", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
`Fungsi copy_file`
```ruby
void copy_file(char *old_file, char *new_file) {
    int status;
    pid_t child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"cp", old_file, new_file, NULL};
        execv("/usr/bin/cp", argv);
    } else {
        ((wait(&status)) > 0);
    }
}
```
<br>Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Sama seperti tadi mengacu pada potongan kode yang ada pada **2B**, berikut merupakan kode spesifik yang digunakan.
```ruby
strcpy(new_file, temp_path);
strcat(new_file, "/");
strcat(new_file, string[0]);
strcat(new_file, ".png");
							
copy_file(old_file, new_file);
							
//new_file (yang akhir)
strcpy(temp_path, path);
strcat(temp_path,"/");
strcat(temp_path,string[5]);
            .
            .							
            .	
strcpy(new_file, temp_path);
strcat(new_file, "/");
strcat(new_file, string[3]);
strcat(new_file, ".png");

move_file(old_file, new_file);		
```
**Algoritma kode**
- Pada kasus dimana 1 file terdapat 2 poster, caranya kurang lebih sama dengan algoritma pada sub soal **2C**. Hal yang membedakan hanyalah pada saat pemindahan poster pertama, fungsi yang digunakan adalah fungsi copy_file (hal ini dilakukan karena file tersebut juga akan digunakan pada saat pemindahan poster kedua). Setelah poster pertama sudah dipindahkan barulah file tersebut dipindahkan menggunakan fungsi move_file untuk poster yang kedua. 

**Nomor 2e**
<br>*Fungsi yang saya buat dan digunakan pada sub soal ini*<br>
`Fungsi input_data`
```ruby
void input_data(char *path, char *judul, char *tahun){
    FILE *fp;
    strcpy(tmp, path);
    strcat(tmp, "/data.txt");
    fp = fopen (tmp, "a+");
    fprintf(fp, "nama : %s\nrilis : %s\n\n\n", judul, tahun);
    fclose(fp);
}
```
Karena pada sub soal **2B** tadi sudah sekaligus membuat file `data.txt` maka yang perlu dilakukan hanyalah membuat list serial dari masing-masing kategori. Mengacu pada kodingan pada sub soal **2B**, kita hanya perlu menggunakan fungsi `input_data` untuk memasukkan data-data sesuai yang diminta pada soal. Contohnya seperti berikut.
```ruby
input_data(temp_path, string[0], string[1]);
```
```ruby
input_data(temp_path, string[3], string[4]);
```

Contoh isi dari file `data.txt`.
![error1](img/3.png)

## Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
a.	Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 
b.	Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
c.	Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
d.	Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
e.	Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png

### Penyelesaian Soal

Pertama, kita inisialisasi beberapa variabel untuk memudahkan proses pengerjaan
```C
char path[50] = "/home/natya/modul2", file[100], tmp[100];
```

**Nomor 3a**
<br> Hal pertama yang dilakukan adalah untuk membuat directory seperti permintaan soal dengan 2 folder yaitu darat dan air, folder air dibuat 3 detik setelah folder darat, maka digunakan fungsi "sleep(3)". Fungsi ini kita beri nama "makedir()"
```C
void makedir(){
	int status;
	pid_t child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", "/home/natya/modul2/darat", NULL};
		execv("/usr/bin/mkdir", argv);
	} 
	else {
		((wait(&status)) > 0);
		child_id = fork();
		if (child_id == 0) {
			sleep(3);
			char *argv[] = {"mkdir", "-p", "/home/natya/modul2/air", NULL};
			execv("/usr/bin/mkdir", argv);
		} 
		else {
			((wait(&status)) > 0);
		}
	}
}
```
Screenshot dari folder yang dibuat :
![error1](img/4.png)

**Nomor 3b**
<br> Pada poin ini kita diminta untuk mengekstrak file "animal.zip" ke directory "“/home/[USER]/modul2/" dan kita ekstrak saja secara simpel. Fungsi ini kita beri nama unzip()
```C

void unzip(){
pid_t child_id;
 	int status;

  	child_id = fork();

  	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
   // this is child
    
    	char *argv[] = {"unzip","-q" ,"/home/natya/animal.zip","-d","/home/natya/modul2", NULL};
      execv("/bin/unzip", argv);
    	
  	} else {
    // this is parent
    	while ((wait(&status)) > 0);
  }
}
```

**Nomor 3c**
<br> Untuk nomor ini kita buat 2 function yang masing2 memiliki kegunaan berbeda, "move_file()" untuk memindahkan file nantinya dan "delete()" untuk menghapus file dari directory yang ditentukan
```C
void move_file(char *old, char *new) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mv", old, new, NULL};
		execv("/usr/bin/mv", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```
```C
void delete(){
	int status;
		pid_t child_id = fork();
		if (child_id == 0) {
			char *argv[] = {"rm","-r","/home/natya/modul2/animal", NULL};
			execv("/usr/bin/rm", argv);
		} else {
			((wait(&status)) > 0);
		}
}
```

**Nomor 3d**
<br> Untuk nomor ini pada dasarnya hanya menggunakan function "delete()"

*Nomor 3e**
<br> Untuk nomor ini kita buat suatu function untuk mendapatkan permission level dari setiap file dan memodifikasi filename tersebut sesuai permission yang dimiliki file, kita membuat 3 fungsi disini yaitu untuk mendapatkan permission, untuk membuat file baru .txt, dan untuk menulis data ke text file tersebut
```C
char * getUID(char *filename){
	 struct stat fs;
    int r;

    r = stat(filename,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }  
    
	 struct passwd *pw = getpwuid(fs.st_uid);
	 strcat(pw->pw_name,"_");

    if( fs.st_mode & S_IRUSR )
        strcat(pw->pw_name,"r");
    if( fs.st_mode & S_IWUSR )
        strcat(pw->pw_name,"w");
    if( fs.st_mode & S_IXUSR )
        strcat(pw->pw_name,"x");
        
	 strcat(pw->pw_name,"_");
	 
    return pw->pw_name;
} 
```
Untuk kode diatas pada dasarnya kita membaca permission yang dimiliki sebuah file dan menggunakan "strcat" untuk memodifikasi nama file sesuai dengan permission yang dimiliki file, dengan menambahkan huruf r,w, dan x sesuai dengan permission file tersebut

```C
void make_file(char *path){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/list.txt");
	fp = fopen (tmp, "a+");
	fclose(fp);
}
```
Function ini kita buat untuk membuat sebuah text file bernama "list.txt" sekaligus membuka file tersebut agar bisa ditulis oleh fungsi berikuntnya

```C
void input_data(char *path, char *uidd, char *fname){
	FILE *fp;
	char sv[50];
	strcpy(sv, path);
	strcat(sv, "list.txt");
	fp = fopen (sv, "a+");
	fprintf(fp, "%s%s\n", uidd, fname);
	fclose(fp);
}
```
Fungsi ini menulis nama file yang telah dimodifikasi menjadi suatu list di text file

Screenshot dari text file tersebut:
![error1](img/6.png)

**Penjalanan Fungsi**
```C
int main(){
	makedir();
	unzip();
	
	make_file("/home/natya/modul2/darat");
	make_file("/home/natya/modul2/air");
	
	DIR *d;
  	struct dirent *dir;
  	d = opendir("/home/natya/modul2/animal");
   	if (d)
   	{
		while ((dir = readdir(d)) != NULL)
		{
			strcpy(file,dir->d_name);
			
			if(strstr(dir->d_name,"darat")){
				if(!(strstr(dir->d_name,"bird"))){
				char old[50] = "/home/natya/modul2/animal/";
				char fpath1[50] = "/home/natya/modul2/darat/";
				strcat(old,file);
				move_file(old,"/home/natya/modul2/darat");
				
				input_data(fpath1,getUID(fpath1),dir->d_name);
				
				}
			}
			else if(strstr(dir->d_name,"air")){
				char old[50] = "/home/natya/modul2/animal/";
				char fpath2[50] = "/home/natya/modul2/air/";
				strcat(old,file);
				move_file(old,"/home/natya/modul2/air");
				
				input_data(fpath2,getUID(fpath2),dir->d_name);
			}
			else
				continue;
			
		}
		delete();
		
	closedir(d);
	}

	return 0;
}
```

## Penjelasan Main Function
1. Kita melakukan pembuatan directory
2. Kita lakukan unzip
3. Kemudian kita buka directory dan memisahkan file sesuai namanya dan memindahkannya ke folder sesuai
4. Setelah di sortir ke folder masing-masing, kita delete file dengan tipe bird di folder awal
5. Kita tutup directory dan selesaikan function tersebut

Screenshot dari file yang telah disorting sesuai nama filenya:
![error1](img/5.png)
