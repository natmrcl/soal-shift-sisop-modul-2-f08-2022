#include<stdio.h>
#include<dirent.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<wait.h>
#include <sys/stat.h>
#include <pwd.h>

char path[50] = "/home/natya/modul2", file[100], tmp[100];

void makedir(){
	int status;
	pid_t child_id = fork();
	
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", "/home/natya/modul2/darat", NULL};
		execv("/usr/bin/mkdir", argv);
	} 
	else {
		((wait(&status)) > 0);
		child_id = fork();
		if (child_id == 0) {
			sleep(3);
			char *argv[] = {"mkdir", "-p", "/home/natya/modul2/air", NULL};
			execv("/usr/bin/mkdir", argv);
		} 
		else {
			((wait(&status)) > 0);
		}
	}
}

void unzip(){
pid_t child_id;
 	int status;

  	child_id = fork();

  	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
   // this is child
    
    	char *argv[] = {"unzip","-q" ,"/home/natya/animal.zip","-d","/home/natya/modul2", NULL};
      execv("/bin/unzip", argv);
    	
  	} else {
    // this is parent
    	while ((wait(&status)) > 0);
  }
}

void move_file(char *old, char *new) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mv", old, new, NULL};
		execv("/usr/bin/mv", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void delete(){
	int status;
		pid_t child_id = fork();
		if (child_id == 0) {
			char *argv[] = {"rm","-r","/home/natya/modul2/animal", NULL};
			execv("/usr/bin/rm", argv);
		} else {
			((wait(&status)) > 0);
		}
}

char * getUID(char *filename){
	 struct stat fs;
    int r;

    r = stat(filename,&fs);
    if( r==-1 )
    {
        fprintf(stderr,"File error\n");
        exit(1);
    }  
    
	 struct passwd *pw = getpwuid(fs.st_uid);
	 strcat(pw->pw_name,"_");

    if( fs.st_mode & S_IRUSR )
        strcat(pw->pw_name,"r");
    if( fs.st_mode & S_IWUSR )
        strcat(pw->pw_name,"w");
    if( fs.st_mode & S_IXUSR )
        strcat(pw->pw_name,"x");
        
	 strcat(pw->pw_name,"_");
	 
    return pw->pw_name;
} 

void make_file(char *path){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/list.txt");
	fp = fopen (tmp, "a+");
	fclose(fp);
}

void input_data(char *path, char *uidd, char *fname){
	FILE *fp;
	char sv[50];
	strcpy(sv, path);
	strcat(sv, "list.txt");
	fp = fopen (sv, "a+");
	fprintf(fp, "%s%s\n", uidd, fname);
	fclose(fp);
}

int main(){
	makedir();
	unzip();
	
	make_file("/home/natya/modul2/darat");
	make_file("/home/natya/modul2/air");
	
	DIR *d;
  	struct dirent *dir;
  	d = opendir("/home/natya/modul2/animal");
   	if (d)
   	{
		while ((dir = readdir(d)) != NULL)
		{
			strcpy(file,dir->d_name);
			
			if(strstr(dir->d_name,"darat")){
				if(!(strstr(dir->d_name,"bird"))){
				char old[50] = "/home/natya/modul2/animal/";
				char fpath1[50] = "/home/natya/modul2/darat/";
				strcat(old,file);
				move_file(old,"/home/natya/modul2/darat");
				
				input_data(fpath1,getUID(fpath1),dir->d_name);
				
				}
			}
			else if(strstr(dir->d_name,"air")){
				char old[50] = "/home/natya/modul2/animal/";
				char fpath2[50] = "/home/natya/modul2/air/";
				strcat(old,file);
				move_file(old,"/home/natya/modul2/air");
				
				input_data(fpath2,getUID(fpath2),dir->d_name);
			}
			else
				continue;
			
		}
		delete();
		
	closedir(d);
	}

	return 0;
}
