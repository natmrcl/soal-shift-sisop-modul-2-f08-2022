#include<stdio.h>
#include<dirent.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<wait.h>

char path[50] = "/home/natya/Modul2/shift2/drakor";
char temp_path[100], old_file[100], new_file[100], tmp[100];
int count;

void move_file(char *old_file, char *new_file) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mv", old_file, new_file, NULL};
		execv("/usr/bin/mv", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void makedir(char *path){
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void make_file(char *path, char *genre){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/data.txt");
	fp = fopen (tmp, "a+");
	fprintf(fp, "kategori : %s\n\n\n", genre);
	fclose(fp);
}

void input_data(char *path, char *judul, char *tahun){
	FILE *fp;
	strcpy(tmp, path);
	strcat(tmp, "/data.txt");
	fp = fopen (tmp, "a+");
	fprintf(fp, "nama : %s\nrilis : %s\n\n\n", judul, tahun);
	fclose(fp);
}
void copy_file(char *old_file, char *new_file) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"cp", old_file, new_file, NULL};
		execv("/usr/bin/cp", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void unzip(){
pid_t child_id;
 	int status;

  	child_id = fork();

  	if (child_id < 0) {
    	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  	}

  	if (child_id == 0) {
   // this is child
    
    	char *argv[] = {"unzip","-q" ,"/home/natya/Modul2/drakor.zip","*.png","-d","/home/natya/Modul2/shift2/drakor", NULL};
      execv("/bin/unzip", argv);
    	
  	} else {
    // this is parent
    	while ((wait(&status)) > 0);
  }
}


int main(void)
{
	makedir(path);
	unzip();
	
    DIR *d,*exists;
    struct dirent *dir;
    char f_name[200], temp[200];
    d = opendir(path);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
        if(strstr(dir->d_name,"png")){
		     		strcpy(f_name, dir->d_name);
		     		strcpy(temp, dir->d_name);
		     		strcpy(temp_path, path);
		     		
		     		//untuk hapus .pngnya
		     		int l = strlen(temp);
		     		for(int i = l, j = 4; j >= 0; j--,i--) temp[i] = '\0';
		     		
		     		char * token = strtok(temp, ";_");
		         //printf("%s\n", f_name);
		         
		         if(!(strstr(dir->d_name,"_"))){
		         	count = 0;
		         	//string[0] nyimpen judul, string[1] nyimpen tahun, string[2] nyimpen genre
		         	char string[3][100];
		         		while(token != NULL){
		         		//printf ("%s ",token);
		         		//biar ga looping
		         		
		         		strcpy(string[count],token);
		         		token = strtok(NULL, ";_");
							count++;
		         	}
		         	strcat(temp_path,"/");
		         	strcat(temp_path,string[2]);
		         
		         	exists = opendir(temp_path);
						if(!exists) {makedir(temp_path);make_file(temp_path, string[2]);}
						input_data(temp_path, string[0], string[1]);
				      	
				  		//old_file
				      	strcpy(old_file, path);
				      	strcat(old_file, "/");
				      	strcat(old_file, f_name);
				      	
				      	//new_file
				      	strcpy(new_file, temp_path);
				      	strcat(new_file, "/");
				      	strcat(new_file, string[0]);
				      	strcat(new_file, ".png");
				      	
				      	
				      	move_file(old_file, new_file);
				        
		         }
		         else if(strstr(dir->d_name,"_")){
		         	count = 0;
		         	//string[0] nyimpen judul, string[1] nyimpen tahun, string[2] nyimpen genre
		         	char string[6][100];
		         		while(token != NULL){
		         		//printf ("%s ",token);
		         		
		         		strcpy(string[count],token);
		         		//biar ga looping
		         		token = strtok(NULL, ";_");
							count++;
		         	}
		         	  				      	
				  		//old_file
				      	strcpy(old_file, path);
				      	strcat(old_file, "/");
				      	strcat(old_file, f_name);
				      	
				      	//new_file (yang awal)
				      	strcat(temp_path,"/");
		         		strcat(temp_path,string[2]);
		         		
		         		exists = opendir(temp_path);
				      	if(!exists) {makedir(temp_path);make_file(temp_path, string[2]);}
							input_data(temp_path, string[0], string[1]);
				      	
				      	strcpy(new_file, temp_path);
				      	strcat(new_file, "/");
				      	strcat(new_file, string[0]);
				      	strcat(new_file, ".png");
				      	
				      	copy_file(old_file, new_file);
				      	
				      	//new_file (yang akhir)
				      	strcpy(temp_path, path);
				      	strcat(temp_path,"/");
		         		strcat(temp_path,string[5]);
		         		
		         		exists = opendir(temp_path);
				      	if(!exists) {makedir(temp_path);make_file(temp_path, string[5]);}
							input_data(temp_path, string[3], string[4]);	
				      	strcpy(new_file, temp_path);
				      	strcat(new_file, "/");
				      	strcat(new_file, string[3]);
				      	strcat(new_file, ".png");

							move_file(old_file, new_file);
							
		         }
		         
		         else
		         	continue;
		     }
		     }
		     closedir(d);
    }
    return(0);
}
